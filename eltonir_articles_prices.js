import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));

const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const articlesDirectory = Deno.env.get("FTP_ARTICLES_DIRECTORY");
const pricesDirectory = Deno.env.get("FTP_PRICES_DIRECTORY");
const processedFilesDirectory = Deno.env.get("FTP_PROCESSED_DIRECTORY");

let skuMap = {};
let articleMap = {};

const genderMap = {
  112: "Niños",
  114: "Niñas",
  115: "Bebés",
  110: "Hombre",
  108: "Niños",
  111: "Mujer",
  106: "Mujer",
  117: "Niños",
  116: "Bebés",
  107: "Hombre",
  109: "Sin Genero",
};

async function init() {
  const client = new FTPClient(ftpHost, {
    user: user,
    pass: pass,
    mode: "passive",
    port: 21,
  });

  try {
    await client.connect();
    console.log("Client connected")
    await processArticleFiles(client);
    console.log("Processed articles")
    await processPriceFiles(client);
    console.log("Processed prices")
    await client.close();
  } catch (e) {
    console.log(`Error: ${e.message}`)
    throw e
  }

  let articles = Object.values(articleMap)

  for (let article of articles) {
    let hasPrice = !article.ecommerce[0].properties.find(x => x.price && x.price.value <= 0)
    let hasVariants = article.ecommerce[0].variants.length

    if (hasPrice && hasVariants) {
      await sagalDispatch(article);
    }
  }
}

async function processArticleFiles(client) {
  let data = await client.list(articlesDirectory);
  let filteredData = data.filter((x) => x.match(/articles_\d+.csv/)).sort();
  if (filteredData?.length) {
    for (let file of filteredData) {
      try {
        let filename = file.split("/").slice(-1);
        let oldestArticles = await client.download(file);

        await client.rename(
          file,
          `${processedFilesDirectory}/${filename}`,
          oldestArticles
        );

        await processArticles(oldestArticles);
      } catch (e) {
        console.log(`Unable to process file: ${file}, message: ${e.message}`);
      }
    }
  }
}

async function processPriceFiles(client) {
  let data = await client.list(pricesDirectory);
  let filteredData = data.filter((x) => x.match(/prices_\d+.csv/)).sort();
  if (filteredData?.length) {
    for (let file of filteredData) {
      try {
        let filename = file.split("/").slice(-1);
        let oldestPrices = await client.download(file);

        await client.rename(
          file,
          `${processedFilesDirectory}/${filename}`,
          oldestPrices
        );

        await processPrices(oldestPrices);
      } catch (e) {
        console.log(`Unable to process file: ${file}, message: ${e.message}`);
      }
    }
  }
}

async function processPrices(oldestPrices) {
  const decoder = new TextDecoder();
  let priceText = decoder.decode(oldestPrices).split("\n");

  for (let i = 1; i < priceText.length; i++) {
    let price = priceText[i];
    if (price && price != "") {
      try {
        let priceToClean = price.split("|");
        let priceFields = [];

        for (let cleanedPrice of priceToClean) {
          priceFields.push(cleanedPrice.replaceAll('"', ""));
        }

        let variantSku = priceFields[0];
        let currency = priceFields[1];
        let priceValue = parseFloat(priceFields[2]);

        let article = articleMap[skuMap[variantSku]];

        for (let ecommerce of article.ecommerce) {
          ecommerce.properties.push({
            price: {
              currency: currency == 1 ? "$" : "U$S",
              value: priceValue,
            }
          })
        }


      } catch (e) {
        console.log(`Error parsing price: ${e.message}`);
      }
    }
  }
}

async function processArticles(articles) {
  const decoder = new TextDecoder();
  let articleText = decoder.decode(articles).split("\n");

  for (let i = 1; i < articleText.length; i++) {
    let article = articleText[i];
    if (article && article != "") {
      try {
        let articleToClean = article.split("|");
        let articleFields = [];

        for (let cleanedArticle of articleToClean) {
          articleFields.push(cleanedArticle.replaceAll('"', ""));
        }

        let sku = articleFields[0].trim();
        let name = articleFields[1].trim();
        let description = articleFields[2];
        let variantSku = articleFields[3];
        let variantColor = articleFields[4];
        let variantSize = articleFields[5];
        let isActive = Number(articleFields[6]);
        let gender = articleFields[8];

        skuMap[variantSku] = sku;

        if (!articleMap[sku]) {
          articleMap[sku] = {
            sku: sku.trim(),
            client_id: clientId,
            integration_id: clientIntegrationId,
            options: {
              merge: false,
            },
            ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
              return {
                ecommerce_id: ecommerce_id,
                properties: [],
                variants: []
              }
            })
          };
        }

        for (let ecommerce of articleMap[sku].ecommerce) {
          ecommerce.properties.push({
            name: name,
          })
          ecommerce.properties.push({
            description: description
          })

          ecommerce.properties.push({
            attributes: {
              Genero: genderMap[gender],
            },
          })


          ecommerce.variants.push({
            sku: variantSku.trim(),
            properties: [
              {
                attributes: {
                  COLOR: variantColor,
                  SIZE: variantSize,
                },
              },
              {
                metadata: {
                  isActive: `${isActive}`
                }
              }
            ],
          })
        }
      } catch (e) {
        console.log(`Error parsing article: ${e.message}`);
      }
    }
  }
}

await init()