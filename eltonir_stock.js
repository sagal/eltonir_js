import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";
import { parse } from "https://deno.land/x/xml@v1.0.2/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const apiUrl = Deno.env.get("INTEGRATOR_API");

let skuMap = {}


async function getVariantStock(variantSku) {
    let payload = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pos="http://genesys.com.uy/POScommWeb/">
            <soapenv:Header/>
            <soapenv:Body>
            <pos:ListaStockArticulos>
                <!--Optional:-->
                <pos:lart>
                    <!--Optional:-->
                    <pos:ListStockActual>
                        <!--Zero or more repetitions:-->
                        <pos:ArtStock>
                        <!--Optional:-->
                        <pos:IdArt>${variantSku}</pos:IdArt>
                        </pos:ArtStock>
                    </pos:ListStockActual>
                </pos:lart>
            </pos:ListaStockArticulos>
            </soapenv:Body>
        </soapenv:Envelope>`;

    let response = await fetch(
        `http://deportesuru.dyndns.org/poscommweb/service.asmx`,
        {
            method: "POST",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                "Content-Type": "text/xml;charset=UTF-8",
                SOAPAction: "http://genesys.com.uy/POScommWeb/ListaStockArticulos",
            },
            body: payload,
        }
    );

    let textResponse = await response.text();

    let parsedResponse = parse(textResponse);

    let parsedResult =
        parsedResponse["soap:Envelope"]["soap:Body"]["ListaStockArticulosResponse"][
        "ListaStockArticulosResult"
        ];

    if (Number(parsedResult["Status"]["Codigo"]) === 0) {
        let variantStock = parsedResult["ListArtStock"]["ArtStock"]["Stock"];
        return Number(variantStock);
    } else {
        console.log(`Stock not defined for this sku: ${variantSku}`);
        return 0
    }
}

async function processEcommerce(ecommerceId) {
    console.log('Fetching products...')
    let productsResponse = await axiod.get(`${apiUrl}/api/client/${clientId}/ecommerce/${ecommerceId}/products`);
    let productsSkus = Object.keys(productsResponse.data)
    console.log(`Products Fetched: ${productsSkus.length}`)
    for (let productSku of productsSkus) {
        try {
            let producResponse = await axiod.post(`${apiUrl}/api/client/${clientId}/ecommerce/${ecommerceId}/products`, { sku: productSku });
            let product = producResponse.data
            await processProduct(product, ecommerceId)
        } catch (e) {
            console.log(`Could not process ${productSku} -> error: ${e.message}`)
        }
    }
}

async function processVariant(variant) {
    let { metadata } = variant.properties.find(x => x.metadata)
    if (!skuMap.hasOwnProperty(variant.sku)) {
        if (metadata["isActive"] == "0") {
            console.log(`Variant not active: ${variant.sku}`)
            skuMap[variant.sku] = 0
        } else {
            skuMap[variant.sku] = await getVariantStock(variant.sku)
        }
    }
    let variantPayload =
    {
        sku: variant.sku.trim(),
        properties: [
            {
                stock: skuMap[variant.sku]
            },
        ]
    }
    return variantPayload
}

async function processProduct(product, ecommerceId) {
    let payload = {
        sku: product.sku.trim(),
        integration_id: clientIntegrationId,
        client_id: clientId,
        options: {
            merge: false,
        },
    }

    let variants = await Promise.all(product.ecommerce[0].variants.map(processVariant))
    let ecommerce = {
        ecommerce_id: ecommerceId,
        properties: [],
        variants: variants,
        options: {
            override_create: {
                "send": false
            },
            override_update: "default"
        }
    }
    payload.ecommerce = [ecommerce]
    await sagalDispatch(payload)
}

async function init() {
    for (let ecommerceId of Object.values(clientEcommerce)) {
        await processEcommerce(ecommerceId)
    }
}
await init()