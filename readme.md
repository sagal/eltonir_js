# Integration script for client: Tonir

## Introduction

This script was created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. Tonir is one of these particular clients, exposing their data in two servers, one FTP server for article and price data and one SOAP server for article stock. 

## Workflow

- The script begins by downloading the existing article identifiers (sku) into memory, then it proceeds to process the awaiting article files in the client's FTP server. 
- This fetched article data is reformatted into a Sagal-friendly structure within a large map, and each article's stock data is consulted through a series of requests to the client's SOAP server. 
- Once the articles have been processed, the script proceeds to process all the awaiting pricing files located in the client's FTP server, downloading them into memory. 
- The price values are loaded into their corresponding articles, be it the ones that were processed earlier or existing products in the database if no article was found. If an article doesn't exist, neither in the processed articles nor the products database, then the price is ignored. 
- With this done, the restructured articles are loaded with both pricing and stock data and dispatched into Sagal to be uploaded into the system. 

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client (In this case, Tonir). A number. 
- **CLIENT_INTEGRATION_ID**: The integration script's id in the existing runtime environment. A number. 
- **CLIENT_ECOMMERCE**: A map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **FTP_HOST**: The client's FTP server address.
- **FTP_USER**: The username required to log into the client's FTP server.
- **FTP_PASSWORD**: The password required to log into the client's FTP server.
- **FTP_ARTICLES_DIRECTORY**: The filepath for the directory holding the client's unprocessed article files. 
- **FTP_PRICES_DIRECTORY**: The filepath for the directory holding the client's unprocessed price files. 
- **FTP_PROCESSED_DIRECTORY**: The filepath for the directory holding the client's processed files. 
- **PRODUCTS_API_URL**: The URL exposing the endpoint where the client's existing products in Sagal's system can be obtained. 

## Functions 

### processCurrentSkus()

**Description:** fetches existing products from the system's exposed API endpoint and loads them into a product map where they are indexed by the sku of their variant, which is used as the main identifier for pricing data in Tonir. 

### processArticleFiles(FTPClient client)

**Description:** processes each file in the client's unprocessed articles directory by calling the processArticles function for every existing article file. In the case of an error processing the articles, the next file is processed without interrupting execution. 

### processArticles(Uint8Array articles) 

**Description:** decodes the data stream containing the article data and restructures it according to Sagal's requirements, uploading to an in-memory map keyed to the sku identifiers. Calls the getVariantStock function to obtain the stock data from the SOAP server for every article that is currently marked as active by the client. 

### getVariantStock(String variantSku)

**Description:** returns the current stock for the article variant passed as a parameter, via a request to the client's SOAP server. 

### processPriceFiles(FTPClient client)

**Description:** processes each file in the client's unprocessed articles directory by calling the processPrices function for every existing pricing file. In the case of an error processing the prices, the next file is processed without interrupting execution.

### processPrices(Uint8Array prices)

**Description:** decodes the data stream containing the pricing data and uploads the restrcutured pricing data to their respective articles in memory. If no matching article is found in memory, an updated instance of the article is created using a matching identifier from the system's existing product database and the price is loaded there. If the price's identifier is not found in either location, the price is skipped and the operation is continued without interruption. 

